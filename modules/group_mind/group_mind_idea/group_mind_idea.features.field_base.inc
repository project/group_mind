<?php
/**
 * @file
 * group_mind_idea.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function group_mind_idea_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_idea_status'
  $field_bases['field_idea_status'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_idea_status',
    'field_permissions' => array(
      'type' => 2,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'open' => 'Open',
        'planned' => 'Planned',
        'completed' => 'Completed',
        'delivered' => 'Delivered',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
