<?php

/**
 * Define Idea blocks.
 */

/**
 * Implements hook_block_info().
 */
function group_mind_idea_block_info() {
  $blocks = array();
  $blocks['create_an_idea'] = array(
    'info' => t('Create an Idea Button'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function group_mind_idea_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'create_an_idea':
      $block['subject'] = '<none>';
      $block['content'] = t('<a class="btn btn-default" href="@create_idea_url">Create an idea</a>', array('@create_idea_url' => url('node/add/idea')));
      break;
  }
  return $block;
}
