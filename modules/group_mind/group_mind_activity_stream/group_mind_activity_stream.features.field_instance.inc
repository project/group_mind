<?php
/**
 * @file
 * group_mind_activity_stream.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function group_mind_activity_stream_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'message-activity_stream-field_activity_stream_body'
  $field_instances['message-activity_stream-field_activity_stream_body'] = array(
    'bundle' => 'activity_stream',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'message',
    'field_name' => 'field_activity_stream_body',
    'label' => 'Activity stream body',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Activity stream body');

  return $field_instances;
}
