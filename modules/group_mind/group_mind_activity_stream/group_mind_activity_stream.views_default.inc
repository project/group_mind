<?php
/**
 * @file
 * group_mind_activity_stream.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function group_mind_activity_stream_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'gm_user_activity';
  $view->description = '';
  $view->tag = 'Group Mind';
  $view->base_table = 'message';
  $view->human_name = 'Activity Streams (User-specific activity)';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Recent site activity';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['link_display'] = 'custom_url';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'This person doesn\'t have any activity on the site yet.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Message: Timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'message';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['label'] = '';
  $handler->display->display_options['fields']['timestamp']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['timestamp']['date_format'] = 'time_ago_dynamic';
  /* Field: Message: Activity stream body */
  $handler->display->display_options['fields']['field_activity_stream_body']['id'] = 'field_activity_stream_body';
  $handler->display->display_options['fields']['field_activity_stream_body']['table'] = 'field_data_field_activity_stream_body';
  $handler->display->display_options['fields']['field_activity_stream_body']['field'] = 'field_activity_stream_body';
  $handler->display->display_options['fields']['field_activity_stream_body']['label'] = '';
  $handler->display->display_options['fields']['field_activity_stream_body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_activity_stream_body']['alter']['text'] = '[field_activity_stream_body-value]';
  $handler->display->display_options['fields']['field_activity_stream_body']['element_label_colon'] = FALSE;
  /* Field: Message: Notification body */
  $handler->display->display_options['fields']['field_notification_body']['id'] = 'field_notification_body';
  $handler->display->display_options['fields']['field_notification_body']['table'] = 'field_data_field_notification_body';
  $handler->display->display_options['fields']['field_notification_body']['field'] = 'field_notification_body';
  $handler->display->display_options['fields']['field_notification_body']['label'] = '';
  $handler->display->display_options['fields']['field_notification_body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_notification_body']['alter']['text'] = '[field_notification_body-value]';
  $handler->display->display_options['fields']['field_notification_body']['element_label_colon'] = FALSE;
  /* Sort criterion: Message: Timestamp */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'message';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
  /* Contextual filter: Message: User uid */
  $handler->display->display_options['arguments']['user']['id'] = 'user';
  $handler->display->display_options['arguments']['user']['table'] = 'message';
  $handler->display->display_options['arguments']['user']['field'] = 'user';
  $handler->display->display_options['arguments']['user']['default_action'] = 'default';
  $handler->display->display_options['arguments']['user']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['user']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['user']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['user']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['user']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Message: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'message';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'activity_stream' => 'activity_stream',
    'visual_notification' => 'visual_notification',
  );
  $export['gm_user_activity'] = $view;

  return $export;
}
