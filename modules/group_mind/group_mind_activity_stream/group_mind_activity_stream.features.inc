<?php
/**
 * @file
 * group_mind_activity_stream.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function group_mind_activity_stream_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function group_mind_activity_stream_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_message_type().
 */
function group_mind_activity_stream_default_message_type() {
  $items = array();
  $items['activity_stream'] = entity_import('message_type', '{
    "name" : "activity_stream",
    "description" : "Activity Stream",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:field_activity_stream_body]",
          "format" : "markdown",
          "safe_value" : "\\u003Cp\\u003E[message:field_activity_stream_body]\\u003C\\/p\\u003E\\n"
        }
      ]
    }
  }');
  return $items;
}
