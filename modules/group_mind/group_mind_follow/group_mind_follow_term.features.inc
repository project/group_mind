<?php
/**
 * @file
 * group_mind_follow_term.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function group_mind_follow_term_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_message_type().
 */
function group_mind_follow_term_default_message_type() {
  $items = array();
  $items['new_node_in_taxonomy_term'] = entity_import('message_type', '{
    "name" : "new_node_in_taxonomy_term",
    "description" : "New node in taxonomy term",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : NULL,
    "message_text" : { "und" : [
        {
          "value" : "[message:field-target-nodes:0:author] has posted a new [message:field-target-nodes:0:type] associated to the tag [message:field-target-terms:0:name]",
          "format" : "filtered_html",
          "safe_value" : "\\u003Cp\\u003E[message:field-target-nodes:0:author] has posted a new [message:field-target-nodes:0:type] associated to the tag [message:field-target-terms:0:name]\\u003C\\/p\\u003E\\n"
        },
        {
          "value" : "Hello [message:user:name],\\r\\n\\r\\n\\u003Ca href=\\u0022[message:field-target-nodes:0:author:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:author:name]\\u003C\\/a\\u003E has posted \\u003Ca href=\\u0022[message:field-target-nodes:0:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:title]\\u003C\\/a\\u003E associated to the tag \\u003Ca href=\\u0022[message:field-target-terms:0:url:absolute]\\u0022\\u003E[message:field-target-terms:0:name]\\u003C\\/a\\u003E that you\\u0027re following.\\r\\n\\r\\nCheers,\\r\\nThe \\u003Ca href=\\u0022[site:url]\\u0022\\u003E[site:name] webmaster\\u003C\\/a\\u003E\\r\\n",
          "format" : "full_html",
          "safe_value" : "Hello [message:user:name],\\n\\n\\u003Ca href=\\u0022[message:field-target-nodes:0:author:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:author:name]\\u003C\\/a\\u003E has posted \\u003Ca href=\\u0022[message:field-target-nodes:0:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:title]\\u003C\\/a\\u003E associated to the tag \\u003Ca href=\\u0022[message:field-target-terms:0:url:absolute]\\u0022\\u003E[message:field-target-terms:0:name]\\u003C\\/a\\u003E that you\\u0027re following.\\n\\nCheers,\\nThe \\u003Ca href=\\u0022[site:url]\\u0022\\u003E[site:name] webmaster\\u003C\\/a\\u003E\\n"
        },
        {
          "value" : "\\u003Ca href=\\u0022[message:field-target-nodes:0:author:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:author:name]\\u003C\\/a\\u003E has posted \\u003Ca href=\\u0022[message:field-target-nodes:0:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:title]\\u003C\\/a\\u003E associated to the tag \\u003Ca href=\\u0022[message:field-target-terms:0:url:absolute]\\u0022\\u003E[message:field-target-terms:0:name]\\u003C\\/a\\u003E that you\\u0027re following.",
          "format" : "full_html",
          "safe_value" : "\\u003Ca href=\\u0022[message:field-target-nodes:0:author:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:author:name]\\u003C\\/a\\u003E has posted \\u003Ca href=\\u0022[message:field-target-nodes:0:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:title]\\u003C\\/a\\u003E associated to the tag \\u003Ca href=\\u0022[message:field-target-terms:0:url:absolute]\\u0022\\u003E[message:field-target-terms:0:name]\\u003C\\/a\\u003E that you\\u0027re following."
        }
      ]
    }
  }');
  return $items;
}
