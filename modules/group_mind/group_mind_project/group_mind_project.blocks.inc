<?php

/**
 * Define Project blocks.
 */

/**
 * Implements hook_block_info().
 */
function group_mind_project_block_info() {
  $blocks = array();
  $blocks['create_a_project'] = array(
    'info' => t('Create a Project'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function group_mind_project_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'create_a_project':
      $block['subject'] = '<none>';
      $block['content'] = t('<a class="btn btn-default" href="@create_project_url">Create a project</a>', array('@create_project_url' => url('node/add/project')));
      break;
  }
  return $block;
}
