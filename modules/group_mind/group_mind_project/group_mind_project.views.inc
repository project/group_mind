<?php

/**
 * @file
 * Views hook api.
 */


/**
 * Implements hook_view_data().
 **/
function group_mind_project_views_data(){

  $data['project']['table']['group'] = t('Group mind project');
  $data['project']['table']['join'] = array(
    '#global' => array(),
  );

  $data['project']['download'] = array(
     'title' => t('Add the download button'),
     'field' => array(
       'handler' => 'group_mind_project_handler_field_download',
       'help' => t('Show the download counter + the download button.'),
     ),
   );

  return $data;
}