<?php
/**
 * @file
 * Code for the Group mind project includes.
 */

/**
 * Function to download a release and increase the download counter.
 * @param object
 * 	$node.
 */
function group_mind_project_download($node){
	$wrapper = entity_metadata_wrapper('node', $node);
	// Increase the current downloads value.
	$wrapper->field_release_downloads = $wrapper->field_release_downloads->value() + 1;
	// Update it.
	$wrapper->save();
	// Start the download.
	$download_url = $wrapper->field_release_download_url->value();
	drupal_goto($download_url['url']);
}

/**
 * Function to test if a project has already a machine name like this.
 */
function __group_mind_project_machine_name_value_exists($value) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'project');
  $query->fieldCondition('field_project_machine_name', 'value', $value);
  $result = $query->execute();
  if (!empty($result['node'])) {
			$node = reset($result['node']);
			return $node->nid;
	}
  else
    return false;
}