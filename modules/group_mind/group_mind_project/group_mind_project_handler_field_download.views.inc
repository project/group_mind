<?php

/**
 * @file
 * Definition of answers_views_handler_new_content.
 */

/**
 * Field handler to display the download count and button.
 *
 * @ingroup views_field_handlers
 */
class group_mind_project_handler_field_download extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array('table' => 'node', 'field' => 'nid');
    $this->additional_fields['downloads'] = array('table' => 'field_data_field_release_downloads', 'field' => 'field_release_downloads_value');
  }

  function query() {
    $this->field_alias = 'download';
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $nid = $this->get_value($values, 'nid');
    $downloads = $this->get_value($values, 'downloads');

    $output = l('Download', 'project/download/release/' . $nid);
    $output .= '<br />';
    $output .= t('%nb_downloads downloads', array('%nb_downloads' => $downloads));

    return $output;
  }
}
