<?php
/**
 * @file
 * group_mind_project.features.inc
 */

/**
 * Implements hook_views_api().
 */
function group_mind_project_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
