README Group Mind Social Login
--------------------------------------

 * Introduction

  With this module you have:
  - Anonymous who have permission to login in with hybridauth.
  - Predefined rules to complete the profile when the user used hybridauth to login
  - Complete Profile module installed to force the user to complete his profile with your required fields.
 
 * Requirements

  Download HybridAuth library (https://github.com/hybridauth/hybridauth) and unpack it into
  "sites/all/libraries/hybridauth" folder

  * Installation

  Just go in the modules section and enable it.

  * Configuration

  After you need to configure each providers you want to enable in /admin/config/people/hybridauth
