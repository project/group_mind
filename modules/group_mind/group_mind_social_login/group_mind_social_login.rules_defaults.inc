<?php
/**
 * @file
 * group_mind_social_login.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function group_mind_social_login_default_rules_configuration() {
  $items = array();
  $items['rules_login_with_a_social_network'] = entity_import('rules_config', '{ "rules_login_with_a_social_network" : {
      "LABEL" : "Login with a social network",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "social networks" ],
      "REQUIRES" : [ "rules", "hybridauth" ],
      "ON" : { "hybridauth_user_insert" : [] },
      "DO" : [
        { "data_set" : {
            "data" : [ "hybridauth-user:field-name-first" ],
            "value" : "[hybridauth:firstName]"
          }
        },
        { "data_set" : {
            "data" : [ "hybridauth-user:field-name-last" ],
            "value" : "[hybridauth:lastName]"
          }
        },
        { "data_set" : {
            "data" : [ "hybridauth-user:field-country" ],
            "value" : [ "hybridauth:country" ]
          }
        },
        { "data_set" : {
            "data" : [ "hybridauth-user:field-phone-number" ],
            "value" : "[hybridauth:phone]"
          }
        },
        { "data_set" : {
            "data" : [ "hybridauth-user:field-state" ],
            "value" : [ "hybridauth:region" ]
          }
        },
        { "data_set" : {
            "data" : [ "hybridauth-user:field-website:url" ],
            "value" : "[hybridauth:webSiteURL]"
          }
        }
      ]
    }
  }');
  $items['rules_login_with_facebook'] = entity_import('rules_config', '{ "rules_login_with_facebook" : {
      "LABEL" : "Login with facebook",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "social networks" ],
      "REQUIRES" : [ "rules", "hybridauth" ],
      "ON" : { "hybridauth_user_login" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "hybridauth:provider" ], "value" : "Facebook" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "hybridauth-user:field-facebook-url:url" ],
            "value" : "[hybridauth:profileURL]"
          }
        }
      ]
    }
  }');
  $items['rules_login_with_linkedin'] = entity_import('rules_config', '{ "rules_login_with_linkedin" : {
      "LABEL" : "Login with LinkedIn",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "social networks" ],
      "REQUIRES" : [ "rules", "hybridauth" ],
      "ON" : { "hybridauth_user_login" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "hybridauth:provider" ], "value" : "LinkedIn" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "hybridauth-user:field-linkedin-url:url" ],
            "value" : "[hybridauth:profileURL]"
          }
        }
      ]
    }
  }');
  $items['rules_login_with_twitter'] = entity_import('rules_config', '{ "rules_login_with_twitter" : {
      "LABEL" : "Login with Twitter",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "social networks" ],
      "REQUIRES" : [ "rules", "hybridauth" ],
      "ON" : { "hybridauth_user_login" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "hybridauth:provider" ], "value" : "Twitter" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "hybridauth-user:field-twitter-url:url" ],
            "value" : "[hybridauth:profileURL]"
          }
        }
      ]
    }
  }');
  return $items;
}
