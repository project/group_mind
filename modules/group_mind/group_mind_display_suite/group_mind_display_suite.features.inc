<?php
/**
 * @file
 * group_mind_display_suite.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function group_mind_display_suite_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
