<?php
/**
 * @file
 * group_mind_q_a.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function group_mind_q_a_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function group_mind_q_a_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
