<?php
/**
 * @file
 * group_mind_q_a.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function group_mind_q_a_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create answers_answer content'.
  $permissions['create answers_answer content'] = array(
    'name' => 'create answers_answer content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create answers_question content'.
  $permissions['create answers_question content'] = array(
    'name' => 'create answers_question content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any answers_answer content'.
  $permissions['delete any answers_answer content'] = array(
    'name' => 'delete any answers_answer content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any answers_question content'.
  $permissions['delete any answers_question content'] = array(
    'name' => 'delete any answers_question content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own answers_answer content'.
  $permissions['delete own answers_answer content'] = array(
    'name' => 'delete own answers_answer content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own answers_question content'.
  $permissions['delete own answers_question content'] = array(
    'name' => 'delete own answers_question content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any answers_answer content'.
  $permissions['edit any answers_answer content'] = array(
    'name' => 'edit any answers_answer content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any answers_question content'.
  $permissions['edit any answers_question content'] = array(
    'name' => 'edit any answers_question content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own answers_answer content'.
  $permissions['edit own answers_answer content'] = array(
    'name' => 'edit own answers_answer content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own answers_question content'.
  $permissions['edit own answers_question content'] = array(
    'name' => 'edit own answers_question content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  $permissions['edit own answers_question content'] = array(
    'name' => 'edit own answers_question content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
