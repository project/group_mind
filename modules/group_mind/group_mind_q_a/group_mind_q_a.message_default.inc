<?php
/**
 * @file
 * hooks for messages in group_mind_q_a_notifications.
 */

/**
 * Implements hook_default_message_type().
 */
function group_mind_q_a_default_message_type() {
  $items = array();
  $items['update_answer'] = entity_import('message_type', '{
    "name" : "update_answer",
    "description" : "Update answer",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "en",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Ca href=\\u0022[message:field-target-nodes:0:author:url]\\u0022\\u003E[message:field-target-nodes:0:author:name]\\u003C\\/a\\u003E has updated his answer on the question \\u003Ca href=\\u0022[message:field-target-nodes:0:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:field_related_question]\\u003C\\/a\\u003E.",
          "format" : "markdown",
          "safe_value" : "\\u003Cp\\u003E\\u003Ca href=\\u0022[message:field-target-nodes:0:author:url]\\u0022\\u003E[message:field-target-nodes:0:author:name]\\u003C\\/a\\u003E has updated his answer on the question \\u003Ca href=\\u0022[message:field-target-nodes:0:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:field_related_question]\\u003C\\/a\\u003E.\\u003C\\/p\\u003E\\n"
        },
        {
          "value" : "[message:field-target-nodes:0:author] has updated his answer on the question [message:field-target-nodes:0:field_related_question].",
          "format" : "markdown",
          "safe_value" : "[message:field-target-nodes:0:author] has updated his answer on the question [message:field-target-nodes:0:field_related_question]."
        },
        {
          "value" : "Hello [message:user:name],\\r\\n\\r\\n\\u003Ca href=\\u0022[message:field-target-nodes:0:author:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:author:name]\\u003C\\/a\\u003E has updated his answer on the question \\u003Ca href=\\u0022[message:field-target-nodes:0:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:field_related_question]\\u003C\\/a\\u003E.\\r\\n\\r\\nCheers,\\r\\nThe \\u003Ca href=\\u0022[site:url]\\u0022\\u003E[site:name] webmaster\\u003C\\/a\\u003E.",
          "format" : "markdown",
          "safe_value" : "Hello [message:user:name],\\n\\n\\u003Ca href=\\u0022[message:field-target-nodes:0:author:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:author:name]\\u003C\\/a\\u003E has updated his answer on the question \\u003Ca href=\\u0022[message:field-target-nodes:0:url:absolute]\\u0022\\u003E[message:field-target-nodes:0:field_related_question]\\u003C\\/a\\u003E.\\n\\nCheers,\\nThe \\u003Ca href=\\u0022[site:url]\\u0022\\u003E[site:name] webmaster\\u003C\\/a\\u003E."
        }
      ]
    },
    "rdf_mapping" : []
  }');
  return $items;
}