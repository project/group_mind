<?php

/**
 * Define Q&A blocks.
 */

/**
 * Implements hook_block_info().
 */
function group_mind_q_a_block_info() {
  $blocks = array();
  $blocks['create_a_question'] = array(
    'info' => t('Create a Question Button'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function group_mind_q_a_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'create_a_question':
      $block['subject'] = '<none>';
      $block['content'] = t('<a class="btn btn-default" href="@create_question_url">Create a question</a>', array('@create_question_url' => url('node/add/answers-question')));
      break;
  }
  return $block;
}
