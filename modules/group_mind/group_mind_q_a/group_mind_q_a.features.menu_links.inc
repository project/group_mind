<?php
/**
 * @file
 * group_mind_q_a.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function group_mind_q_a_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_qa:questions-and-answers.
  $menu_links['main-menu_qa:questions-and-answers'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'questions-and-answers',
    'router_path' => 'questions-and-answers',
    'link_title' => 'Q&A',
    'options' => array(
      'identifier' => 'main-menu_qa:questions-and-answers',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables.
  // Included for use with string extractors like potx.
  t('Q&A');


  return $menu_links;
}
