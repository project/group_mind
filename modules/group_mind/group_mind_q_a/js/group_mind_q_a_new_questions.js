/**
 * @file
 * JavaScript to check the presence of new questions and update the user's view.
 */

jQuery(document).ready(function(){
    // Global variable with the last nid.
    last_nid = Drupal.settings.group_mind_q_a.last_nid_question;
    // Load the function that check that there are new questions created.
    setInterval(function(){
        // Get if there are new questions.
        jQuery.get('/answers/new/' + last_nid, NULL, check_new_questions);
    },1000);

    // Function to check if there is new questions.
    var check_new_questions = function(response){
        if (response) {

            // A new question.
            if (response.number > 0) {
                // Refresh latest questions.
                jQuery("#block-views-gm-q-a-block-lastest").fadeOut("slow").load(window.location + " #block-views-gm-q-a-block-lastest").fadeIn('slow');
                // Set the global variable last nid with the new nid.
                last_nid = response.questions[0].nid;
                // Warn the user that there is a new question, only if the user is on the all tab.
                if (jQuery('#alert-new-question').length == 0) {
                    jQuery("<div id='alert-new-question' class='alert alert-info'><span class='glyphicon glyphicon-refresh'></span>New questions have been posted. Click here to see</div>").fadeIn("slow").insertBefore('#block-system-main .views-qa-master .view-content');
                    // Refresh if the user click.
                    jQuery('#alert-new-question').click(function(e){
                        e.preventDefault();
                        // Reload the Q&A.
                        jQuery("#block-system-main  .views-qa-master").fadeOut("fast").load(window.location + ' #block-system-main .views-qa-master > *', function(response, status, xhr) {
                            if (status == "success") {
                                 jQuery(".views-qa-master").fadeIn('fast', function(){
                                    panel = 'page_qa';

                                    // Get all class we will work on it.
                                    var classes = jQuery(this).attr('class');
                                    // Get the dom id.
                                    var domid = classes.match(/view-dom-id-([0-9a-z]*)/);

                                    // Set the good ajax for views.
                                    Drupal.settings.views.ajaxViews = {};
                                    index_domid = { value: 'views_dom_id:' + domid[1] };
                                    Drupal.settings.views.ajaxViews[index_domid.value] = {
                                        page_element: '0',
                                        view_args: '',
                                        view_base_path: 'questions-and-answers',
                                        view_display_id: panel,
                                        view_dom_id: domid[1],
                                        view_name: "gm_q_a",
                                        view_path: "questions-and-answers"
                                    };

                                    // Force Jquery to work.
                                    var onceClass = classes.match(/jquery-once-[0-9]*-[a-z]*/);

                                    if (onceClass != NULL && onceClass[0] != '') {
                                      jQuery(this).removeClass(onceClass[0]);
                                    }

                                    Drupal.attachBehaviors(jQuery(this), Drupal.settings);
                                 });
                            }
                       });
                     });
                }
            }
        }
    }
});
