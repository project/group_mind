<?php
/**
 * @file
 * group_mind_text_format.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function group_mind_text_format_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
