<?php
/**
 * @file
 * group_mind_blog.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function group_mind_blog_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function group_mind_blog_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function group_mind_blog_node_info() {
  $items = array(
    'blog_article' => array(
      'name' => t('Blog article'),
      'base' => 'node_content',
      'description' => t('Create here your articles for your blog'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
