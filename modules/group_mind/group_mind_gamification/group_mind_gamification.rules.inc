<?php
/**
 * @file
 * group_mind_gamification.rules.inc
 */

/**
 * implements hook_rules_condition_info
 */
function group_mind_gamification_rules_condition_info() {
  $conditions = array();

  $conditions['rules_condition_first_project_release'] = array(
    'label' => t('First project release'),
    'group' => 'Gamification',
    'base' => 'group_mind_gamification_rules_condition_first_project_release',
    'parameter' => array(
      'release' => array(
        'label' => t('Release'),
        'type'  => 'entity',
      )
    )
  );

  return $conditions;
}

/**
 * implements hook_rules_action_info
 */
function group_mind_gamification_rules_action_info() {
  $actions = array();

  $actions['log_gamification_events'] = array(
    'label'     => t('Log gamification events & trigger Action set'),
    'group'     => 'Gamification',
    'base'      => 'group_mind_gamification_rules_action_log_gamification_events',
    'parameter' => array(
      'count' => array(
        'label' => t('Count'),
        'type'  => 'integer',
        'description' => t('How many times should this action be triggered before triggering the Action set'),
      ),
      'user' => array(
        'type'  => 'user',
        'label' => t('User'),
      ),
      'entity' => array(
        'label' => t('Entity'),
        'type'  => 'entity',
      ),
      'action_set' => array(
        'label'       => t('Action set'),
        'type'        => 'text',
        'description' => t('The action set to trigger after count is reached'),
        'options list' => 'group_mind_gamification_action_set_list',
      ),
    ),
  );

  return $actions;
}

/**
 * Condition: First project release
 *
 * @param EntityDrupalWrapper $release
 */
function group_mind_gamification_rules_condition_first_project_release($release) {
  if ('project_release' !== $release->getBundle()) {
    return false;
  }

  $pid = $release->field_release_project->nid->value();

  $query = new EntityFieldQuery();
  $result = (int) $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'project_release')
    ->fieldCondition('field_release_project', 'target_id', $pid)
    ->count()
    ->execute()
  ;

  return 1 == $result ?: false;
}

/**
 * Action: Log gamification events & trigger action set
 *
 * @param int $count
 * @param unknown_type $user
 * @param unknown_type $entity
 * @param text $actionSet
 */
function group_mind_gamification_rules_action_log_gamification_events($count, $user, $entity, $actionSet) {
  _group_mind_gamification_log_activity($user, $entity, $actionSet);

  // Count not reached for this action set
  if (!_group_mind_gamification_count_achieved((int) $count, $user, $entity, $actionSet)) {
    return;
  }

  // Action set has already been triggered
  if (_group_mind_actionset_triggered($actionSet, $user)) {
    return;
  }

  rules_invoke_component($actionSet, $user);
  _group_mind_log_triggered_actionset($actionSet, $user);
}

/**
 * Returns the list of Action set tagged with Gamification
 */
function group_mind_gamification_action_set_list() {
  $options = array();

  $actionSets = entity_load('rules_config', FALSE, array('plugin' => 'action set', 'active' => TRUE));
  foreach($actionSets as $actionSet) {
    if (in_array('Gamification', $actionSet->tags)) {
      $options[$actionSet->name] = $actionSet->label;
    }
  }

  return $options;
}

/**
 * Log gamification activity
 *
 * @param stdClass $user
 * @param EntityDrupalWrapper $entity
 * @param string $actionSet
 */
function _group_mind_gamification_log_activity($user, $entity, $actionSet) {
  db_merge('gamification_activity')
    ->key(array(
      'entity_id'     => $entity->getIdentifier(),
      'entity_type'   => $entity->type(),
      'entity_bundle' => $entity->getBundle(),
      'uid'           => $user->uid,
      'action_set'    => $actionSet,
    ))
    ->fields(array('timestamp' => time()))
    ->execute()
  ;
}

/**
 * Check if count if achived for the user/entity/actionSet
 *
 * @param int $count
 * @param stdClass $user
 * @param EntityDrupalWrapper $entity
 * @param string $actionSet
 */
function _group_mind_gamification_count_achieved($count, $user, $entity, $actionSet) {
  $query = db_select('gamification_activity');

  $query
    ->condition('entity_type', $entity->type())
    ->condition('entity_bundle', $entity->getBundle())
    ->condition('uid', $user->uid)
    ->condition('action_set', $actionSet)
  ;

  return $count == (int) $query->countQuery()->execute()->fetchField();
}


/**
 * Log that an actionset has been triggered for the specified user
 *
 * @param string   $actionSet
 * @param stdClass $user
 */
function _group_mind_log_triggered_actionset($actionSet, $user) {
  $query = db_insert('gamification_actionset_triggered')
    ->fields(array(
      'action_set' => $actionSet,
      'uid'        => $user->uid,
    ))
  ;

  $query->execute();
}

/**
 * Check if the specified actionset has been triggered for this user
 *
 * @param string $actionSet
 * @param stdClass $user
 */
function _group_mind_actionset_triggered($actionSet, $user) {
  $query = db_select('gamification_actionset_triggered');
  $query->condition('action_set', $actionSet);
  $query->condition('uid', $user->uid);

  return (boolean) $query->countQuery()->execute()->fetchField();
}
