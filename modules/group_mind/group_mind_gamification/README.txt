Information:

This module is a work in progress, and as such may be prone to bugs.

This module gives you the opportunity to choose how many points are attributed or removed on specific actions
(i.e. Creation of a question, Creation of an answer, Upvote of a question)

The current state of the module forces you to configure these values at the beggining
of the file 'group_mind_gamification.rules_defaults.inc'.


Known bugs / TODO list:

    - When editing content, do not reattribute points
    - Add translation
    - New releases points get attributed twice 