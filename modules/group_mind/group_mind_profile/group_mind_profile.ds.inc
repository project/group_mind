<?php
/**
 * @file
 * group_mind_profile.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function group_mind_profile_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'user|user|full';
  $ds_fieldsetting->entity_type = 'user';
  $ds_fieldsetting->bundle = 'user';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'activity_stream' => array(
      'weight' => '14',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:15:{s:23:"override_pager_settings";i:0;s:9:"use_pager";i:1;s:14:"nodes_per_page";s:2:"20";s:8:"pager_id";s:1:"0";s:6:"offset";s:1:"0";s:9:"more_link";i:0;s:10:"feed_icons";i:0;s:10:"panel_args";i:0;s:12:"link_to_view";i:0;s:4:"args";s:0:"";s:3:"url";s:0:"";s:7:"display";s:7:"default";s:7:"context";a:1:{i:0;s:29:"argument_entity_id:user_1.uid";}s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}s:4:"type";s:5:"views";s:7:"subtype";s:16:"gm_user_activity";}',
        'load_terms' => 0,
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'prefix' => '<h3>Recent site activity</h3>',
          'fi' => TRUE,
          'fi-el' => 'div',
          'fi-cl' => 'well',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
          'fi-first-last' => FALSE,
        ),
      ),
    ),
    'badges_list' => array(
      'weight' => '8',
      'label' => 'above',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb' => 'Badges',
          'lb-el' => 'h4',
          'prefix' => '<hr />',
        ),
      ),
    ),
    'bpm_points' => array(
      'weight' => '2',
      'label' => 'above',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'BPM',
        ),
      ),
    ),
    'name' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h4',
        'class' => '',
        'ft' => array(),
      ),
    ),
    'ds_user_picture' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'ds_picture_avatar',
    ),
  );
  $export['user|user|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function group_mind_profile_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'activity_stream';
  $ds_field->label = 'Activity stream';
  $ds_field->field_type = 7;
  $ds_field->entities = array(
    'user' => 'user',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'default' => array(),
    'settings' => array(
      'show_title' => array(
        'type' => 'checkbox',
      ),
      'title_wrapper' => array(
        'type' => 'textfield',
        'description' => 'Eg: h1, h2, p',
      ),
      'ctools' => array(
        'type' => 'ctools',
      ),
    ),
  );
  $export['activity_stream'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'badges_list';
  $ds_field->label = 'badges list';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'user' => 'user',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?php
if(preg_match_all(\'#user/([0-9]+)#\',$_GET[\'q\'], $matches)){
    $uid = $matches[1][0];
    echo user_badges_userweight_page(user_load($uid));
}
?>',
      'format' => 'php_code',
    ),
    'use_token' => 0,
  );
  $export['badges_list'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'bpm_points';
  $ds_field->label = 'BPM points';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'user' => 'user',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?php
if(preg_match_all(\'#user/([0-9]+)#\',$_GET[\'q\'], $matches)){
    $uid = $matches[1][0];
    echo userpoints_get_max_points($uid);
}
?>',
      'format' => 'php_code',
    ),
    'use_token' => 0,
  );
  $export['bpm_points'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function group_mind_profile_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'user|user|full';
  $ds_layout->entity_type = 'user';
  $ds_layout->bundle = 'user';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'ds_user_picture',
        1 => 'name',
        2 => 'bpm_points',
        3 => 'field_linkedin_url',
        4 => 'field_facebook_url',
        5 => 'field_twitter_url',
        6 => 'field_company',
        7 => 'field_website',
        8 => 'badges_list',
      ),
      'right' => array(
        9 => 'field_name_first',
        10 => 'field_name_last',
        11 => 'summary',
        12 => 'field_bio',
        13 => 'field_country',
        14 => 'activity_stream',
      ),
    ),
    'fields' => array(
      'ds_user_picture' => 'left',
      'name' => 'left',
      'bpm_points' => 'left',
      'field_linkedin_url' => 'left',
      'field_facebook_url' => 'left',
      'field_twitter_url' => 'left',
      'field_company' => 'left',
      'field_website' => 'left',
      'badges_list' => 'left',
      'field_name_first' => 'right',
      'field_name_last' => 'right',
      'summary' => 'right',
      'field_bio' => 'right',
      'field_country' => 'right',
      'activity_stream' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'col-xs-3' => 'col-xs-3',
        'text-center' => 'text-center',
      ),
      'right' => array(
        'col-xs-9' => 'col-xs-9',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 1,
  );
  $export['user|user|full'] = $ds_layout;

  return $export;
}
