<?php
/**
 * @file
 * group_mind_notifications.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function group_mind_notifications_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function group_mind_notifications_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_message_type().
 */
function group_mind_notifications_default_message_type() {
  $items = array();
  $items['visual_notification'] = entity_import('message_type', '{
    "name" : "visual_notification",
    "description" : "Visual notification",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "en",
    "arguments" : null,
    "message_text" : { "en" : [
        {
          "value" : "\\u003Cp\\u003E[message:field-notification-body]\\u003C\\/p\\u003E",
          "format" : "wysiwyg",
          "safe_value" : "\\u003Cp\\u003E[message:field-notification-body]\\u003C\\/p\\u003E"
        }
      ]
    }
  }');
  return $items;
}

/**
 * Implements hook_flag_default_flags().
 */
function group_mind_notifications_flag_default_flags() {
  $flags = array();
  // Exported flag: "Follow for visual notification".
  $flags['follow_for_visual_notification'] = array(
    'entity_type' => 'node',
    'title' => 'Follow for visual notification',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Follow',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unfollowing',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'group_mind_notifications',
    'locked' => array(
      'name' => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_image_default_styles().
 */
function group_mind_notifications_image_default_styles() {
  $styles = array();

  // Exported image style: avatar.
  $styles['avatar'] = array(
    'name' => 'avatar',
    'label' => 'Avatar',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 128,
          'height' => 128,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_block_info.
 **/
function group_mind_notifications_block_info() {
  // The block to display the account + notifications.
  $blocks['group_mind_notifications_block'] = array(
    'info' => t('Account & Notifications icon'),
    'status' => TRUE,
    'region' => 'menu',
    'visibility' => 2,
    'cache' => DRUPAL_NO_CACHE
  );

  return $blocks;
}

/**
 * Implements hook_block_info.
 */
function group_mind_notifications_block_view($delta = '') {
  // The $delta parameter tells us which block is being requested.
  switch ($delta) {
    case 'group_mind_notifications_block':
      // Hide if the user is not logged
      if (!user_is_logged_in()) {
        $block['content'] = theme('group_mind_notifications_icon_anonymous');
      }else{
        // Load user data.
        global $user;
        // User personal informations
        $user_data = user_load($user->uid);
        $block['content'] =  theme('group_mind_notifications_icon', array('user_data' => $user_data));

      }
    break;
  }
  return $block;
}
