<?php
  // Avatar
  $avatar = '';

  if (!empty($user_data->picture->uri)) {
      $avatar = 'background-image: url('.image_style_url('avatar', $user_data->picture->uri).');';
  }
  // Firstname
  $firstname = '';
  if (!empty($user_data->field_user_firstname[LANGUAGE_NONE][0]['safe_value'])) {
    $firstname = $user_data->field_user_firstname[LANGUAGE_NONE][0]['safe_value'];
  }

  // Lastname
  $lastname = '';
  if (!empty($user_data->field_user_lastname[LANGUAGE_NONE][0]['safe_value'])) {
    $lastname = $user_data->field_user_lastname[LANGUAGE_NONE][0]['safe_value'];
  }
?>

<ul class="nav navbar-nav navbar-right">
    <li id="menu-user" class="dropdown medium">
        <a id="user_menu-opener" href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="avatar circle tile-32" style="<?php echo $avatar; ?>"></span>
            <?php echo group_mind_notifications_theme_notifications_number(); ?>
        </a>
        <ul id="user_menu-content" class="dropdown-menu" role="menu">
            <li id="user_menu-content_top">

                <a href="<?php echo url('user/'.$user->uid.'/edit'); ?>" class="avatar pull-left" style="<?php echo $avatar; ?>">
                  <span><?php echo t('Change picture'); ?></span>
                </a>

                <?php if (!empty($firstname) || !empty($lastname)) { ?>
                <b><?php echo $firstname . ' ' . $lastname; ?></b><br />
                <?php } ?>

                <?php if (!empty($user_data->mail)) { ?>
                <small><i><?php echo $user_data->mail; ?></i></small><br />
                <?php } ?>

                <a class="classic" href="<?php echo url('user'); ?>"><?php echo t('My account'); ?></a>

                <div class="clearfix"></div>
            </li>
            <li class="header"><?php echo t('Notifications'); ?></li>
            <li id="visual_notifications_list">
              <?php echo group_mind_notifications_theme_notifications_list(); ?>                       
            </li>
            <li id="user_menu-content_bottom">
                <a class="btn btn-default pull-right" href="<?php echo url('user/logout'); ?>"><?php echo t('Logout'); ?></a>
                <div class="clearfix"></div>
            </li>
        </ul>
    </li>
</ul>
