<ul class="nav navbar-nav navbar-right">
    <li id="menu-user" class="dropdown medium">
        <a id="user_menu-opener" href="<?php echo url('user/login'); ?>">
            <span class="avatar circle tile-32"></span>
        </a>
    </li>
</ul>