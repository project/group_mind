jQuery( document ).ready(function( $ ) {

  //Set the current notifications number.
  current_notifications = Drupal.settings.group_mind_notifications.current_notifications;
  /**
   * Add an extra function to the Drupal ajax object
   * which allows us to trigger an ajax response without
   * an element that triggers it.
   */
  Drupal.ajax.prototype.specifiedResponse = function() {
    var ajax = this;

    try {
      $.ajax(ajax.options);
    }
    catch (err) {
      alert('An error occurred while attempting to process ' + ajax.options.url);
      return false;
    }

    return false;
  };

  Drupal.behaviors.group_mind_notifications = {
    attach: function (context, settings) {
      $(document).delegate('#notification-modal', 'displayNotifications', function(ev, data) {
        if (typeof settings.current_notifications != 'undefined') {
          current_notifications = settings.current_notifications;
        }
        $("#notification-modal").removeClass("off");

        // Close the notifications box after 3 seconds.
        if (typeof closeNotificationsBox != 'undefined') {
          //If already defined the timeout, reset it (we can add notifications)
          clearTimeout(closeNotificationsBox);
        }
        closeNotificationsBox = setTimeout(function(){close_notifications_box();}, 20000);

      });
    }
  }

  /**
   * Refresh the number of notifications.
   */
  function ajax_update_notifications(){
    /**
      * Define a custom ajax action not associated with an element.
      */
     var custom_settings = {};
     custom_settings.url = '/group_mind_notifications/ajax/update/' +  current_notifications;
     custom_settings.event = 'onload';
     custom_settings.keypress = false;
     custom_settings.prevent = false;
     Drupal.ajax['ajax_update_notifications'] = new Drupal.ajax(null, $(document.body), custom_settings);
     Drupal.ajax['ajax_update_notifications'].specifiedResponse();
  }

  setInterval(ajax_update_notifications, 10000);

  /**
  * Reset Notifications number when we click on the notification icon.
  **/
  $('#notifications').parent().click('beforeShow', function() {
     /**
      * Define a custom ajax action not associated with an element.
      */
     var custom_settings = {};
     custom_settings.url = '/group_mind_notifications/ajax/reset' ;
     custom_settings.event = 'onload';
     custom_settings.keypress = false;
     custom_settings.prevent = false;
     Drupal.ajax['ajax_reset_notifications'] = new Drupal.ajax(null, $(document.body), custom_settings);
     Drupal.ajax['ajax_reset_notifications'].specifiedResponse();
     current_notifications = 0;
  })

  /**
   * Close the notifications box.
   */
  function close_notifications_box(){
    $("#notification-modal").addClass("off");
    $("#notification-modal .notification-content").html('');
  }

  $(".notification-close").click(function(e){
    e.preventDefault();
    close_notifications_box();
  });

});