<?php
/**
 * @file
 * group_mind_notifications.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function group_mind_notifications_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_notifications_number'.
  $permissions['create field_notifications_number'] = array(
    'name' => 'create field_notifications_number',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_notifications_number'.
  $permissions['edit field_notifications_number'] = array(
    'name' => 'edit field_notifications_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_notifications_number'.
  $permissions['edit own field_notifications_number'] = array(
    'name' => 'edit own field_notifications_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_notifications_number'.
  $permissions['view field_notifications_number'] = array(
    'name' => 'view field_notifications_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_notifications_number'.
  $permissions['view own field_notifications_number'] = array(
    'name' => 'view own field_notifications_number',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
