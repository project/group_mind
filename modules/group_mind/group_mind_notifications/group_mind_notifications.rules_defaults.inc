<?php
/**
 * @file
 * group_mind_notifications.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function group_mind_notifications_default_rules_configuration() {
  $items = array();
  $items['rules_add_a_visual_notification_about_new_answer'] = entity_import('rules_config', '{ "rules_add_a_visual_notification_about_new_answer" : {
      "LABEL" : "Add a visual notification about new answer",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "visual notification" ],
      "REQUIRES" : [ "flag", "rules" ],
      "ON" : { "node_insert--answers_answer" : { "bundle" : "answers_answer" } },
      "DO" : [
        { "flag_fetch_users_node" : {
            "USING" : {
              "flag" : "follow_for_visual_notification",
              "node" : [ "node:answers-related-question" ]
            },
            "PROVIDE" : { "users" : { "followers" : "Followers" } }
          }
        },
        { "list_remove" : { "list" : [ "followers" ], "item" : [ "node:author" ] } },
        { "LOOP" : {
            "USING" : { "list" : [ "followers" ] },
            "ITEM" : { "follower" : "follower" },
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "message",
                    "param_type" : "visual_notification",
                    "param_user" : [ "follower" ]
                  },
                  "PROVIDE" : { "entity_created" : { "visual_notifiation_created" : "Created visual notification" } }
                }
              },
              { "data_set" : {
                  "data" : [ "visual-notifiation-created:field-notification-body" ],
                  "value" : "\\u003Ca href=\\u0022[node:author:url]\\u0022\\u003E[node:author]\\u003C\\/a\\u003E added an answer to the question \\u003Ca href=\\u0022[node:answers-related-question:url]#node-[node:nid]\\u0022\\u003E[node:answers-related-question]\\u003C\\/a\\u003E"
                }
              },
              { "data_set" : {
                  "data" : [ "visual-notifiation-created:field-notification-author" ],
                  "value" : [ "node:author" ]
                }
              },
              { "data_set" : {
                  "data" : [ "follower:field-notifications-number" ],
                  "value" : {
                    "select" : "follower:field-notifications-number",
                    "num_offset" : { "value" : "1" }
                  }
                }
              },
              { "entity_save" : { "data" : [ "visual-notifiation-created" ], "immediate" : 1 } }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_add_a_visual_notification_about_new_answer_comment'] = entity_import('rules_config', '{ "rules_add_a_visual_notification_about_new_answer_comment" : {
      "LABEL" : "Add a visual notification about new answer comment",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "visual notification" ],
      "REQUIRES" : [ "rules", "flag", "comment" ],
      "ON" : { "comment_insert--comment_node_answers_answer" : { "bundle" : "comment_node_answers_answer" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "comment:node" ], "field" : "answers_related_question" } }
      ],
      "DO" : [
        { "flag_fetch_users_node" : {
            "USING" : {
              "flag" : "follow_for_visual_notification",
              "node" : [ "comment:node:answers-related-question" ]
            },
            "PROVIDE" : { "users" : { "followers" : "Followers" } }
          }
        },
        { "list_remove" : { "list" : [ "followers" ], "item" : [ "comment:author" ] } },
        { "LOOP" : {
            "USING" : { "list" : [ "followers" ] },
            "ITEM" : { "follower" : "follower" },
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "message",
                    "param_type" : "visual_notification",
                    "param_user" : [ "follower" ]
                  },
                  "PROVIDE" : { "entity_created" : { "visual_notifiation_created" : "Created visual notification" } }
                }
              },
              { "data_set" : {
                  "data" : [ "visual-notifiation-created:field-notification-body" ],
                  "value" : "\\u003Ca href=\\u0022[comment:author:url]\\u0022\\u003E[comment:author]\\u003C\\/a\\u003E added \\u003Ca href=\\u0022[comment:node:url]#comment-[comment:cid]\\u0022\\u003E a comment\\u003C\\/a\\u003E on the question:  \\u003Ca href=\\u0022[comment:node:url]\\u0022\\u003E[comment:node:answers_related_question]\\u003C\\/a\\u003E"
                }
              },
              { "data_set" : {
                  "data" : [ "visual-notifiation-created:field-notification-author" ],
                  "value" : [ "comment:author" ]
                }
              },
              { "entity_save" : { "data" : [ "visual-notifiation-created" ], "immediate" : 1 } },
              { "data_set" : {
                  "data" : [ "follower:field-notifications-number" ],
                  "value" : {
                    "select" : "follower:field-notifications-number",
                    "num_offset" : { "value" : "1" }
                  }
                }
              }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_add_a_visual_notification_about_new_comment'] = entity_import('rules_config', '{ "rules_add_a_visual_notification_about_new_comment" : {
      "LABEL" : "Add a visual notification about new comment",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "visual notification" ],
      "REQUIRES" : [ "rules", "flag", "comment" ],
      "ON" : { "comment_insert" : [] },
      "IF" : [
        { "NOT entity_is_of_bundle" : {
            "entity" : [ "comment:node" ],
            "type" : "node",
            "bundle" : { "value" : { "answers_answer" : "answers_answer" } }
          }
        }
      ],
      "DO" : [
        { "flag_fetch_users_node" : {
            "USING" : { "flag" : "follow_for_visual_notification", "node" : [ "comment:node" ] },
            "PROVIDE" : { "users" : { "followers" : "Users who need a visual notification" } }
          }
        },
        { "list_remove" : { "list" : [ "followers" ], "item" : [ "comment:author" ] } },
        { "LOOP" : {
            "USING" : { "list" : [ "followers" ] },
            "ITEM" : { "follower" : "Current follower" },
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "message",
                    "param_type" : "visual_notification",
                    "param_user" : [ "follower" ]
                  },
                  "PROVIDE" : { "entity_created" : { "visual_notifiation_created" : "Created visual notification" } }
                }
              },
              { "data_set" : {
                  "data" : [ "visual-notifiation-created:field-notification-body" ],
                  "value" : "[comment:author] added a new comment on the [comment:node:type]: \\u003Ca href=\\u0022[comment:node:url]#comment-[comment:cid]\\u0022\\u003E[comment:node]\\u003C\\/a\\u003E"
                }
              },
              { "data_set" : {
                  "data" : [ "visual-notifiation-created:field-notification-author" ],
                  "value" : [ "comment:author" ]
                }
              },
              { "data_set" : {
                  "data" : [ "follower:field-notifications-number" ],
                  "value" : {
                    "select" : "follower:field-notifications-number",
                    "num_offset" : { "value" : "1" }
                  }
                }
              }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_add_the_follow_visual_notification_on_each_answer'] = entity_import('rules_config', '{ "rules_add_the_follow_visual_notification_on_each_answer" : {
      "LABEL" : "Add the follow visual notification on each answer",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "visual notification" ],
      "REQUIRES" : [ "flag", "rules" ],
      "ON" : { "node_insert--answers_answer" : { "bundle" : "answers_answer" } },
      "DO" : [
        { "flag_flagnode" : {
            "flag" : "follow_for_visual_notification",
            "node" : [ "node:answers-related-question" ],
            "flagging_user" : [ "node:author" ],
            "permission_check" : 1
          }
        }
      ]
    }
  }');
  $items['rules_add_the_follow_visual_notification_on_each_answer_comment'] = entity_import('rules_config', '{ "rules_add_the_follow_visual_notification_on_each_answer_comment" : {
      "LABEL" : "Add the follow visual notification on each answer comment",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "visual notification" ],
      "REQUIRES" : [ "rules", "flag", "comment" ],
      "ON" : { "comment_insert--comment_node_answers_answer" : { "bundle" : "comment_node_answers_answer" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "comment:node" ], "field" : "answers_related_question" } }
      ],
      "DO" : [
        { "flag_flagnode" : {
            "flag" : "follow_for_visual_notification",
            "node" : [ "comment:node:answers-related-question" ],
            "flagging_user" : [ "comment:author" ],
            "permission_check" : 1
          }
        }
      ]
    }
  }');
  $items['rules_add_the_follow_visual_notification_on_each_comment'] = entity_import('rules_config', '{ "rules_add_the_follow_visual_notification_on_each_comment" : {
      "LABEL" : "Add the follow visual notification on each comment",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "visual notification" ],
      "REQUIRES" : [ "rules", "flag", "comment" ],
      "ON" : { "comment_insert" : [] },
      "IF" : [
        { "NOT entity_is_of_bundle" : {
            "entity" : [ "comment:node" ],
            "type" : "node",
            "bundle" : { "value" : { "answers_answer" : "answers_answer" } }
          }
        }
      ],
      "DO" : [
        { "flag_flagnode" : {
            "flag" : "follow_for_visual_notification",
            "node" : [ "comment:node" ],
            "flagging_user" : [ "comment:author" ],
            "permission_check" : "1"
          }
        }
      ]
    }
  }');
  $items['rules_add_the_follow_visual_notification_on_each_node'] = entity_import('rules_config', '{ "rules_add_the_follow_visual_notification_on_each_node" : {
      "LABEL" : "Add the follow visual notification on each node",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "visual notification" ],
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "NOT entity_is_of_bundle" : {
            "entity" : [ "node" ],
            "type" : "node",
            "bundle" : { "value" : { "answers_answer" : "answers_answer" } }
          }
        }
      ],
      "DO" : [
        { "flag_flagnode" : {
            "flag" : "follow_for_visual_notification",
            "node" : [ "node" ],
            "flagging_user" : [ "node:author" ],
            "permission_check" : "1"
          }
        }
      ]
    }
  }');
  return $items;
}
