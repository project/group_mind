<?php

/**
 * Custom the display of the documentation section
 */
function group_mind_preprocess_links(&$variables) {
  // Add child button of book.
  if (!empty($variables['links']['book_add_child'])) {
    $variables['links']['book_add_child']['attributes']['class'][] = 'btn';
    $variables['links']['book_add_child']['attributes']['class'][] = 'btn-primary';
  }
}
