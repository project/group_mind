/**
 * Tools
 */
(function($)
{
  /**
   * Iframe autoresize
   */
  $.fn.iframeAutoresize = function()
  {
      this.each(function() {
          var $video    = $(this),
              newWidth  = 0,
              newHeight = 0;

          // Resize iframe using its original width and height
          if ('' !== this.height && '' !== this.width) {
              newWidth  = parseInt($video.parent().width());
              newHeight = parseInt(newWidth * this.height / this.width);
              $video.width('100%');
              $video.height(newHeight);

          // Resize iframe in 16/9
          } else {
              newWidth  = parseInt($video.parent().width());
              newHeight = parseInt(newWidth * 0.5625);
              $video.width('100%');
              $video.height(newHeight);
          }
      });
  };

  var iframeSelectors = 'iframe.autoresize, section iframe';

  $(document).ready(function() {
    if ($(iframeSelectors).length) {
        $(iframeSelectors).iframeAutoresize();
    }

    $(window).resize(function() {
        if ($(iframeSelectors).length) {
            setTimeout(function(){
                $(iframeSelectors).iframeAutoresize();
            }, 200);
        }
    });
  });

})(jQuery);
