<div id="answer-<?php echo $row->nid;?>" class="panel panel-default">
  <div class="panel-body">
    <div class="row">
          <div class="col-xs-2 col-md-1 text-center gm-qa-kpi">
            <div>
              <span class="gm-qa-kpi-number"><?php echo(empty($row->votingapi_cache_node_points_sum_value) ? '0' : $row->votingapi_cache_node_points_sum_value) ?> </span>
              <br />
              <span class="gm-qa-kpi-title">
                <?php echo t('Votes'); ?>
              </span>
            </div>
          </div>
          <?php
            if(!empty($row->flagging_node_timestamp))
              $class_resolved = 'alert-success';
            else
              $class_resolved = '';
          ?>
          <div class="col-xs-2 col-md-1 text-center gm-qa-kpi  <?php echo $class_resolved; ?>">
             <div>
              <span class="gm-qa-kpi-number"><?php echo ($row->answers_related_question_node_nid); ?></span>
              <br />

              <span class="gm-qa-kpi-title">
               <?php echo t('Answers'); ?>
              </span>
             </div>
          </div>
          <div class="col-xs-8 col-md-10 gm-qa-detail">
              <div class="row">
                  <div class="gm-qa-title">
                    <h3><a href="<?php echo url('node/' . $row->nid);?>"><?php echo $row->node_title;?></a> <?php echo $fields['new_answers_since_last_login']->content ;?></h3>
                  </div>
                  <?php if(!empty($fields['field_question_tags']->content)) : ?>
                      <div class="gm-qa-tags pull-left">
                          <?php echo $fields['field_question_tags']->content;?>
                      </div>
                  <?php endif; ?>
                  <div class="clearfix-mobile"></div>
                  <div class="qa-author pull-right text-right">
                        <div>
                          <?php echo t('Last activity: !last_activity - ' , array('!last_activity' => $fields['changed']->content)); ?>
                          <a href="<?php echo url('user/' . $row->users_node_uid);?>">
                            <?php echo $row->users_node_name;?>
                            </a>
                        </div>
                        <?php if(!empty($row->users_userpoints_total__userpoints_total_points)) : ?>
                            <div>
                              <?php echo t('!number_points !points', array('!number_points' => $row->users_userpoints_total__userpoints_total_points) + userpoints_translation()); ?>
                            </div>

                        <?php endif; ?>
                      </div>
              </div>
          </div>
    </div>
  </div>
</div>
