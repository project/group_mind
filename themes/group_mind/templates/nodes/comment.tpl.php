<?php
/**
 * @file
 * Default theme implementation for comments.
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $created: Formatted date and time for when the comment was created.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->created variable.
 * - $changed: Formatted date and time for when the comment was last changed.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->changed variable.
 * - $new: New comment marker.
 * - $permalink: Comment permalink.
 * - $submitted: Submission information created from $author and $created during
 *   template_preprocess_comment().
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $title: Linked title.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - comment: The current template type, i.e., "theming hook".
 *   - comment-by-anonymous: Comment by an unregistered user.
 *   - comment-by-node-author: Comment by the author of the parent node.
 *   - comment-preview: When previewing a new or edited comment.
 *   The following applies only to viewers who are registered users:
 *   - comment-unpublished: An unpublished comment visible only to administrators.
 *   - comment-by-viewer: Comment by the user currently viewing the page.
 *   - comment-new: New comment since last the visit.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * These two variables are provided for context:
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_comment()
 * @see template_process()
 * @see theme_comment()
 *
 * @ingroup themeable
 */
?>
<div class="gm-comment">
	<div class="gm-author text-right">
		<div class="pull-left">
			<span class="glyphicon glyphicon-comment"></span>
		</div>
		<em><?php print($submitted);?></em>
	</div>
	<div class="content <?php print $classes; ?>"<?php print $content_attributes; ?>>
		<?php
		// Remove reply link to force to disable threading
		unset($content['links']['comment']['#links']['comment-reply']);

		print render($content['comment_body']);
		?>
		<div class="pull-right comment-actions">
			<span class="glyphicon glyphicon-tag"></span> <a href="<?php echo url('node/' . $comment->nid, array('fragment' => 'comment-' . $comment->cid)); ?>"><?php echo t('Permalink'); ?></a>
			<?php

			//Author and admin can edit the comment
			if(comment_access('edit', $comment)):
			?>
				<span class="glyphicon glyphicon-pencil">
				</span>
				<a href="<?php echo url('comment/' . $comment->cid . '/edit') ?>"><?php echo t('Edit') ?></a>
			<?php
			endif;
			//Antispam links ?
			foreach($content['links']['comment']['#links'] as $key_link => $link_value){
				if(preg_match('/^antispam/', $key_link)) :
				    //Choose the good icon
				    switch($key_link){
					case 'antispam_comment_spam' :
					    $glyphicon = 'glyphicon glyphicon-ban-circle';
					break;
					case 'antispam_comment_ham' :
					    $glyphicon = 'glyphicon glyphicon-ok-circle';
					break;
					case 'antispam_comment_publish' :
					    $glyphicon = 'glyphicon glyphicon-eye-open';
					break;
					case 'antispam_comment_unpublish' :
					    $glyphicon = 'glyphicon glyphicon-eye-close';
					break;
				    }
				?>
				    <span class="glyphicon <?php echo $glyphicon; ?>">
				    </span>
				    <a href="<?php echo url($link_value['href']);?>"><?php echo $link_value['title']; ?></a>
				<?php endif;
			} ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
