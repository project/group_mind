/******************************************************************
* Write the CSS in the Group Mind theme
*******************************************************************/

We don't write the css directly . Our CSS uses the Sass method with the SCSS
syntax : http://sass-lang.com/guide

So first of all you need to install all the necessary to compile all SCSS files
present in the CSS directory.

Requirements :
- Bootstrap SASS : https://github.com/twbs/bootstrap-sass (You will find
  installation instructions there)

Suggestions :
- Install compass (on Ubuntu : apt-get install ruby-compass)
- Install the Koala Software which can compile each time you save:
  http://koala-app.com/
- Config Koala in Settings > Compass and Use the System Compass compiler
- Give the sass directory to Koala, it will detect config file and so be ready
  to use !